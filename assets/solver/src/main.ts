import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";

export const main = async () => {
  const prefix = process.env.PREFIX || "api";

  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(prefix);

  await app.listen(
    Number(process.env.PORT) || 8080,
    process.env.HOST || "0.0.0.0"
  );
};

if (require.main === module) {
  (async () => {
    await main();
  })();
}
