import { Module } from "@nestjs/common";
import { AppV1Controller } from "./app.controller";
import { AppService } from "./app.service";

@Module({
  controllers: [AppV1Controller],
  providers: [AppService],
})
export class AppModule {}
