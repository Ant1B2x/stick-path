import { Test } from "@nestjs/testing";
import request from "supertest";
import { StatusCodes } from "http-status-codes";
import { INestApplication } from "@nestjs/common";
import { execPath } from "process";
import { Type } from "class-transformer";

describe("AppModule", () => {
  let app: INestApplication;

  beforeEach(async () => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { AppModule } = require("./app.module");
    const module = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = module.createNestApplication();
    app.init();
  });

  describe("v1", () => {
    it("should return a not found error", async () => {
      await request(app.getHttpServer())
        .get("/v1/doesnotexist")
        .expect(StatusCodes.NOT_FOUND);
    });
  });

  describe("v1", () => {
    const input1: string = `5 7 
      A B C 
      | | | 
      |-| | 
      | |-| 
      | |-| 
      | | | 
      1 2 3 `;
    it("should return A2 B1 C3", async () => {
      await request(app.getHttpServer())
        .post("/v1/solve")
        .set('Content-Type', 'text/plain')
        .send(input1)
        .expect(StatusCodes.CREATED)
        .then((response) => {
          expect(response.text).toEqual("A2\nB1\nC3");
        });
    });
  });

  describe("v1", () => {
    const input2: string = `15 18
    P Q R S T U V W 
    | | | | |-| | | 
    | | |-| | | |-| 
    | |-| |-| | | | 
    |-| |-| | | |-| 
    |-| | | | |-| | 
    | |-| | |-| |-| 
    | | | |-| |-| | 
    |-| | | |-| | | 
    | | |-| | | | | 
    | | | |-| | |-| 
    | | | | |-| | | 
    |-| | | | | | | 
    |-| |-| | | |-| 
    | |-| | |-| | | 
    | | |-| | | |-| 
    |-| |-| | |-| | 
    1 2 3 4 5 6 7 8 `;
    it("should return P3 Q7 R8 S5 T6 U2 V4 W1", async () => {
      await request(app.getHttpServer())
        .post("/v1/solve")
        .set('Content-Type', 'text/plain')
        .send(input2)
        .expect(StatusCodes.CREATED)
        .then((response) => {
          expect(response.text).toEqual("P3\nQ7\nR8\nS5\nT6\nU2\nV4\nW1");
        });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
