import { Injectable } from "@nestjs/common";

@Injectable()
export class AppService {
  async solve(graph: string): Promise<string> {
    const lines = graph.split("\n").map((line) => line.trim());

    const dimensions = this.parseHeader(lines[0]);

    const firstLine = lines[1].split("");
    const lastLine = lines[lines.length - 1].split("");

    // Process cursor from first line to last line containing - and | characters
    let cursor = [...firstLine];
    for (const line of lines.slice(2, -1)) {
      cursor = this.permute(dimensions.size, cursor, line);
    }

    return firstLine
      .map((char) => char + lastLine[cursor.indexOf(char)])
      .join("")
      .replace(/  /g, "\n")
      .trim();
  }

  private parseHeader(header: string): { size: number; height: number } {
    const [width, height] = header.split(" ").map((str) => parseInt(str));
    return {
      size: (width + 1) / 2,
      height,
    };
  }

  private permute(size: number, cursor: string[], line: string): string[] {
    const chars = line.split("");
    chars.forEach((char: string, index: number) => {
      switch (char) {
        case "-":
          [cursor[index - 1], cursor[index + 1]] = [
            cursor[index + 1],
            cursor[index - 1],
          ];
          break;
        default:
          return;
      }
    });
    return cursor;
  }
}
