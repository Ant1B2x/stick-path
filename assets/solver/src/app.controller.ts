import {
  Controller,
  HttpException,
  Post,
  createParamDecorator,
  ExecutionContext,
  BadRequestException,
} from "@nestjs/common";
import { StatusCodes } from "http-status-codes";
import { AppService } from "./app.service";
import getRawBody from "raw-body";

export const PlainBody = createParamDecorator(
  async (_, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<import("express").Request>();
    if (!req.readable) {
      throw new BadRequestException("Invalid body");
    }

    const body = (await getRawBody(req)).toString("utf8").trim();
    return body;
  }
);

@Controller("/v1")
export class AppV1Controller {
  constructor(private readonly appService: AppService) {}

  @Post("/solve")
  async solve(@PlainBody() graph: string): Promise<string> {
    try {
      return this.appService.solve(graph);
    } catch (e) {
      console.error(e);
      throw new HttpException(e, StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }
}
